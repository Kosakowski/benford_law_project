Benford law project - the probability distribution of the occurrence of a specific first digit in the statistical data, in the case of this project by checking the column `7_2009`.

In the `files_and_credentials` folder, there are data for a sample import and validation check, as well as access data to the admin panel (file: credentials).

Project works with docker:   
`docker-compose build`    
`docker-compose up`   
project is under local address: `127.0.0.1:8002`   
