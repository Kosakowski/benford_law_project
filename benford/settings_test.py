from .settings_base import *


class DisableMigrations(object):
    """
    Based on
    https://gist.github.com/NotSqrt/5f3c76cd15e40ef62d09
    and
    https://simpleisbetterthancomplex.com/tips/2016/08/19/django-tip-12-disabling-migrations-to-speed-up-unit-tests.html

    Disable migrations
    """

    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return None


MIGRATION_MODULES = DisableMigrations()

IS_TEST = True
