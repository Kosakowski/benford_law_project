from datetime import datetime

import factory
from .. import models


class ImportDataFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.ImportData
    person = factory.Sequence(lambda n: 'username_{}'.format(n))
    date = datetime(2020, 8, 1, 10, 0, 0, 0)


class DataItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.DataItem

    import_data = factory.SubFactory(ImportDataFactory)
    state = 'Alabama'
    town = 'Adamsville'
    value_1 = 3
    value_2 = 1
    value_3 = 3
    value_4 = 7.83099
