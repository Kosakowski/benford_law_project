from django.test import TestCase
from .factories import ImportDataFactory, DataItemFactory
from ..models import ImportData

from django.urls import reverse


class DataViewTest(TestCase):
    def test_chart_view(self):
        ImportDataFactory.create(person='Mark')
        ImportDataFactory.create()
        ImportDataFactory.create()
        ImportDataFactory.create()
        url = reverse('data:chart')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(ImportData.objects.count(), 4)
        self.assertContains(response, 'Mark')
        self.assertNotContains(response, 'There is no data in database, please import some data.')

    def test_chart_empty_view(self):
        url = reverse('data:chart')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'There is no data in database, please import some data.')

    def test_benford_data(self):
        import_data = ImportDataFactory.create(person='Mark')
        DataItemFactory(import_data=import_data, value_1=1)
        DataItemFactory(import_data=import_data, value_1=4)
        DataItemFactory(import_data=import_data, value_1=9)
        DataItemFactory(import_data=import_data, value_1=16)
        DataItemFactory(import_data=import_data, value_1=25)
        DataItemFactory(import_data=import_data, value_1=36)
        DataItemFactory(import_data=import_data, value_1=49)
        DataItemFactory(import_data=import_data, value_1=64)
        DataItemFactory(import_data=import_data, value_1=81)
        url = reverse('data:chart')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, [22.22, 11.11, 11.11, 22.22, 0.00, 11.11, 0.00, 11.11, 11.11])
        self.assertContains(response, [30.10, 17.61, 12.49, 9.69, 7.92, 6.69, 5.80, 5.12, 4.58])

    def test_person_detail_view(self):
        import_data = ImportDataFactory.create(person='Mark')
        DataItemFactory(import_data=import_data)
        url = reverse('data:person_report', kwargs={'slug': import_data.slug, 'pk': import_data.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
