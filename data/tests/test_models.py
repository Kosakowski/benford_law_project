from django.test import TestCase
from .factories import ImportDataFactory, DataItemFactory
from ..models import ImportData, DataItem


class TestData(TestCase):
    def test_import_data(self):
        obj = ImportDataFactory.create(person='Tom')
        self.assertEqual(ImportData.objects.count(), 1)
        self.assertEqual(obj.person, 'Tom')

    def test_data_item(self):
        import_data = ImportDataFactory.create()
        obj = DataItemFactory.create(import_data=import_data)
        DataItemFactory.create(import_data=import_data)
        DataItemFactory.create(import_data=import_data)
        DataItemFactory.create(import_data=import_data)
        self.assertEqual(DataItem.objects.count(), 4)
        self.assertEqual(obj.state, 'Alabama')
        self.assertEqual(obj.town, 'Adamsville')
