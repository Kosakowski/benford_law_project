from django.db import models
from django.urls import reverse
from django.utils.text import slugify


class ImportData(models.Model):
    person = models.CharField(max_length=255, unique=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.person

    @property
    def slug(self):
        return slugify(self.person)

    def get_absolute_url(self):
        return reverse('data:person_report', kwargs={'slug': self.slug, 'pk': self.pk})


class DataItem(models.Model):
    import_data = models.ForeignKey(ImportData, related_name='dataitems_importdata', on_delete=models.SET_NULL,
                                    null=True, blank=True)
    state = models.CharField(max_length=255)
    town = models.CharField(max_length=255)
    value_1 = models.IntegerField(help_text='7_2009', blank=True, null=True)
    value_2 = models.IntegerField(help_text='value_2', blank=True, null=True)
    value_3 = models.IntegerField(help_text='value_3', blank=True, null=True)
    value_4 = models.FloatField(help_text='value_4', blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.state, self.town)
