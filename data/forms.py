from django import forms


class UploadFileForm(forms.Form):
    name_of_person = forms.CharField(max_length=100)
    file = forms.FileField()
