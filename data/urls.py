from django.urls import path
from data import views

app_name = 'data'

urlpatterns = [
    path('', views.ChartView.as_view(), name='chart'),
    path('import', views.ImportView.as_view(), name='import'),
    path('person/<slug:slug>/<int:pk>', views.PersonReportDetailView.as_view(), name='person_report')
]
