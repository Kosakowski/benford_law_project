import csv
import io
from collections import Counter
from math import log10

from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, DetailView
from data import forms
from data import models


class ChartView(TemplateView):
    template_name = 'data/chart.html'

    @staticmethod
    def first_number(value):
        for i in value:
            yield int(str(i)[0])

    @staticmethod
    def get_7_2009(data):
        c_dict = Counter(data)
        size = sum(c_dict.values())
        result = [c_dict[k] / size if size else 0 for k in range(1, 10)]
        return result

    def get_context_data(self, **kwargs):
        last_import_data = models.ImportData.objects.last()
        context = super().get_context_data(**kwargs)
        context['results'] = [round(i*100, 2) for i in self.get_7_2009(self.first_number(last_import_data.dataitems_importdata.values_list("value_1", flat=True)))] if last_import_data else None
        context['expected'] = [round(log10(1 + 1 / d)*100, 2) for d in range(1, 10)]
        context['labels'] = list(range(1, 10))
        context['person'] = last_import_data.person if last_import_data else None
        context['personal_reports'] = models.ImportData.objects.all().order_by('-date')
        return context


class ImportView(FormView):
    template_name = 'data/import.html'
    form_class = forms.UploadFileForm
    success_url = reverse_lazy('data:chart')

    @staticmethod
    def valid_headers(word):
        return [char.isalpha() or char.isalnum() or '_' in char for char in word]

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.form_class)
        file = request.FILES['file']
        if form.is_valid():
            data_set = file.read().decode('UTF-8')
            io_string = io.StringIO(data_set)

            d_reader = csv.DictReader(io_string, delimiter='\t')
            headers = d_reader.fieldnames
            if not all(self.valid_headers(headers)):
                messages.warning(request, 'Please check headers in uploading file. Can be numbers data there.')
                return self.form_invalid(form)

            importdata = models.ImportData.objects.create(person=form.cleaned_data['name_of_person'])
            for index, column in enumerate(csv.reader(io_string, delimiter='\t')):
                try:
                    models.DataItem.objects.create(import_data=importdata, state=column[0], town=column[1],
                                                    value_1=column[2], value_2=column[3], value_3=column[4],
                                                    value_4=column[5])
                except ValueError:
                    messages.warning(request, 'Data can be in incorrect table.')
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['personal_reports'] = models.ImportData.objects.all().order_by('-date')
        return context


class PersonReportDetailView(DetailView):
    model = models.ImportData
    template_name = 'data/person_report.html'

    @staticmethod
    def first_number(value):
        for i in value:
            yield int(str(i)[0])

    @staticmethod
    def get_7_2009(data):
        c_dict = Counter(data)
        size = sum(c_dict.values())
        result = [c_dict[k] / size if size else 0 for k in range(1, 10)]
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['personal_reports'] = models.ImportData.objects.all().order_by('-date')
        context['results'] = [round(i*100, 2) for i in self.get_7_2009(self.first_number(self.get_object().dataitems_importdata.values_list("value_1", flat=True)))]
        context['expected'] = [round(log10(1 + 1 / d)*100, 2) for d in range(1, 10)]
        context['labels'] = list(range(1, 10))
        return context
