from django.contrib import admin
from . import models


@admin.register(models.ImportData)
class ImportDataAdmin(admin.ModelAdmin):
    pass


@admin.register(models.DataItem)
class DataItemAdmin(admin.ModelAdmin):
    pass
